# GNU Icecat 

Leia esse artigo em [português](LEIA-ME.md)

Tutorial for installing and removing the Icecat web browser on the Debian GNU/Linux operating system

64-bit environment:

- [Debian](https://debian.org) 10.4.0
- [Icecat](https://www.gnu.org/software/gnuzilla) 60.7.0

## Installation 

Downloading

```
# wget -O wget -O /tmp/icecat.tar.bz2 \
    "https://mirror.nbtelecom.com.br/gnu/gnuzilla/60.7.0/icecat-60.7.0-gnu1.tar.bz2"
```

Extracting

```
# tar -C /opt/ -jxvf /tmp/icecat.tar.bz2
```

Make it accessible from the console

```
# ln -s /opt/icecat/icecat /usr/bin/icecat
```

Create a shortcut, create the file `/usr/share/applications/gnu-icecat.desktop` and add the content below

```
[Desktop Entry]
Name=GNU Icecat
Comment=Web Browser
Exec=/opt/icecat/icecat %u
Terminal=false
Type=Application
Icon=/opt/icecat/browser/chrome/icons/default/default128.png
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/vnd.mozilla.xul+xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
```

### Optional

Add to the list of browsers

```
# update-alternatives --install /usr/bin/x-www-browser \
    x-www-browser /opt/icecat/icecat 200
```

Set as default browser

```
# update-alternatives --set x-www-browser /opt/icecat/icecat
```

Or select as your default browser

```
# update-alternatives --config x-www-browser
```

## Removal 

Remove files and directories

```
# rm /usr/bin/icecat /usr/share/applications/gnu-icecat.desktop
# rm -r /opt/icecat
```

Remove from the list of browsers

```
# update-alternatives --remove x-www-browser /opt/icecat/icecat
```
