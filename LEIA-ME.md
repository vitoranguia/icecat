# GNU Icecat 

Read this article in [english](README.md)

Tutorial de instalação e remoção do navegador web Icecat no sistema operacional Debian GNU/Linux

Ambiente 64 bits:

- [Debian](https://debian.org) 10.4.0
- [Icecat](https://www.gnu.org/software/gnuzilla) 60.7.0

## Instalação

Baixando

```
# wget -O wget -O /tmp/icecat.tar.bz2 \
    "https://mirror.nbtelecom.com.br/gnu/gnuzilla/60.7.0/icecat-60.7.0-gnu1.tar.bz2"
```

Extraindo

```
# tar -C /opt/ -jxvf /tmp/icecat.tar.bz2
```

Torne acessível pelo console

```
# ln -s /opt/icecat/icecat /usr/bin/icecat
```

Crie um atalho, crie o arquivo `/usr/share/applications/gnu-icecat.desktop` e adicione o conteúdo abaixo

```
[Desktop Entry]
Name=GNU Icecat
Comment=Web Browser
Exec=/opt/icecat/icecat %u
Terminal=false
Type=Application
Icon=/opt/icecat/browser/chrome/icons/default/default128.png
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/vnd.mozilla.xul+xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
```

### Opcional

Adicione na lista de navegadores

```
# update-alternatives --install /usr/bin/x-www-browser \
    x-www-browser /opt/icecat/icecat 200
```

Defina como navegador padrão

```
# update-alternatives --set x-www-browser /opt/icecat/icecat
```

Ou selecione como navegador padrão

```
# update-alternatives --config x-www-browser
```

## Remoção

Remova arquivos e diretórios

```
# rm /usr/bin/icecat /usr/share/applications/gnu-icecat.desktop
# rm -r /opt/icecat
```

Remova da lista de navegadores

```
# update-alternatives --remove x-www-browser /opt/icecat/icecat
```
